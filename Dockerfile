FROM openjdk:11

COPY /target/assignment*.jar assignment.jar

ENV DB_PROFILE=h2

CMD java -jar -Dserver.port=8070 -Dspring.profiles.active=$DB_PROFILE assignment.jar

EXPOSE 8070


